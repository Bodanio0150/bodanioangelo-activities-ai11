package com.example.popquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button trueButton;
    private Button falseButton;
    private TextView questionTextView;
    private Object text;
    private ImageButton prevButton;
    private  ImageButton nextButton;

    private int currentQuestionIndex = 0;


    private Questions[] questionsBank = new Questions[]{
            new Questions(R.string.question_Mobile, true),
            new Questions(R.string.question_Android, false),
            new Questions(R.string.question_developed, true),
            new Questions(R.string.question_Appstore, true),
            new Questions(R.string.question_Apps, false),
            new Questions(R.string.question_Popular, false),
            new Questions(R.string.question_Facebook, true),
            new Questions(R.string.question_Instagram, false),
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        falseButton = findViewById(R.id.false_button);
        trueButton = findViewById(R.id.true_button);
        questionTextView = findViewById(R.id.answer_text_view);
        prevButton = findViewById(R.id.prev_button);
        nextButton = findViewById(R.id.next_button);

        trueButton.setOnClickListener(this);
        falseButton.setOnClickListener(this);
        prevButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.true_button:
                checkAnswer(true);
                break;

            case R.id.false_button:
                checkAnswer(false);
                break;

            case R.id.next_button:
                currentQuestionIndex = (currentQuestionIndex + 1) % questionsBank.length;
                updateQuestion();
                break;

            case R.id.prev_button:
                if(currentQuestionIndex > 0 ){
                    currentQuestionIndex = (currentQuestionIndex - 1) % questionsBank.length;
                    updateQuestion();


                 }

        }

    }
    private void updateQuestion(){
        Log.d("current", "onclick" + currentQuestionIndex);
        questionTextView.setText(questionsBank[currentQuestionIndex].getAnswerResId());

    }
    private void checkAnswer(boolean userChoosenCorrect){
        boolean answerIsTrue = questionsBank[currentQuestionIndex].isAnswerTrue();

        int toastMessageId = 0;
        if (userChoosenCorrect == answerIsTrue){
            toastMessageId = R.string.Correct_answer;

        }
        else{
            toastMessageId = R.string.Wrong_answer;
        }
        Toast.makeText(MainActivity.this,toastMessageId,Toast.LENGTH_SHORT).show();

    }
}